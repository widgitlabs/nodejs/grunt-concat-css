/**
 * @widgitlabs/grunt-concat-css
 * https://gitlab.com/widgitlabs/nodejs/grunt-concat-css/
 *
 * Copyright (c) 2021 Widgit Labs
 * Copyright (c) 2013 Olivier Amblet
 *
 * Licensed under the MIT license.
 */

'use strict';

var grunt = require('grunt');

/*
  ======== A Handy Little Nodeunit Reference ========
  https://github.com/caolan/nodeunit
  Test methods:
    test.expect(numAssertions)
    test.done()
  Test assertions:
    test.ok(value, [message])
    test.equal(actual, expected, [message])
    test.notEqual(actual, expected, [message])
    test.deepEqual(actual, expected, [message])
    test.notDeepEqual(actual, expected, [message])
    test.strictEqual(actual, expected, [message])
    test.notStrictEqual(actual, expected, [message])
    test.throws(block, [error], [message])
    test.doesNotThrow(block, [error], [message])
    test.ifError(value)
*/

exports.concat_css = {
  setUp: function(done) {
    // setup here if necessary
    done();
  },

  default_options: function(test) {
    var actual = grunt.file.read('tmp/default_options.css').split('\n');
    var expected = grunt.file.read('tests/expected/default_options.css').split('\n');
    test.deepEqual(actual, expected, 'The two files should be concatenated together.');
    test.done();
  },

  rebase_urls: function(test) {
    var actual = grunt.file.read('tmp/rebase_urls.css').split('\n');
    var expected = grunt.file.read('tests/expected/rebase_urls.css').split('\n');
    test.deepEqual(actual, expected, 'All URLs should have been rebased');
    test.done();
  },

  basedir: function(test) {
    var actual = grunt.file.read('tmp/basedir_options.css').split('\n');
    var expected = grunt.file.read('tests/expected/basedir_options.css').split('\n');
    test.deepEqual(actual, expected, 'All URLs should have been rebased');
    test.done();
  },

  no_basedir_options: function(test) {
    var actual = grunt.file.read('tmp/no_basedir_options.css').split('\n');
    var expected = grunt.file.read('tests/expected/no_basedir_options.css').split('\n');
    test.deepEqual(actual, expected, 'All URLs should have been rebased');
    test.done();
  },

  single_charset: function(test) {
    var actual = grunt.file.read('tmp/single_charset.css').split('\n');
    var expected = grunt.file.read('tests/expected/charset.css').split('\n');
    test.deepEqual(actual, expected, 'All URLs should have been rebased');
    test.done();
  },

  multiple_charsets: function(test) {
    var actual = grunt.file.read('tmp/multiple_charsets.css').split('\n');
    var expected = grunt.file.read('tests/expected/multiple_charsets.css').split('\n');
    test.deepEqual(actual, expected, 'All URLs should have been rebased');
    test.done();
  },

  multiple_charsets_different_order: function(test) {
    var actual = grunt.file.read('tmp/multiple_charsets_different_order.css').split('\n');
    var expected = grunt.file.read('tests/expected/multiple_charsets_different_order.css').split('\n');
    test.deepEqual(actual, expected, 'All URLs should have been rebased');
    test.done();
  },

  multiple_charsets_single_file: function(test) {
    var actual = grunt.file.read('tmp/multiple_charsets_single_file.css').split('\n');
    var expected = grunt.file.read('tests/expected/multiple_charsets_single_file.css').split('\n');
    test.deepEqual(actual, expected, 'All URLs should have been rebased');
    test.done();
  }
};