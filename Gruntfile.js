/**
 * @widgitlabs/grunt-concat-css
 * https://gitlab.com/widgitlabs/nodejs/grunt-concat-css/
 *
 * Copyright (c) 2021 Widgit Labs
 * Copyright (c) 2013 Olivier Amblet
 *
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

  // Load multiple grunt tasks using globbing patterns
  require('load-grunt-tasks')(grunt);

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
  
    // Run JSHint.
    jshint: {
      options: {
        jshintrc: '.jshintrc',
      },
      with_overrides: {
        files: {
          src: ['**/*.js'],
        },
      },
    },

    // Run MarkdownLint.
    markdownlint: {
      full: {
        options: {
          config: {
            default: true,
          },
        },
        src: [
          '**/*.md',
          '!out/**/*.md',
          '!node_modules/**/*.md',
          '!vendor/**/*.md',
          '!**/CHANGELOG.md',
        ],
      },
      changelog: {
        options: {
          config: {
            default: true,
            MD022: false,
            MD024: false,
            MD032: false,
          },
        },
        src: [
          '**/CHANGELOG.md',
          '!out/**/*.md',
          '!node_modules/**/*.md',
          '!vendor/**/*.md',
        ],
      },
    },

    // Run cleanup of temp directory.
    clean: {
      tests: ['tmp']
    },

    // Run concat_css.
    concat_css: {
      default_options: {
        options: {
        },
        files: {
          'tmp/default_options.css': ['tests/fixtures/test.css', 'tests/fixtures/import.css', 'tests/fixtures/component_a/css/style.css']
        }
      },
      baseDir: {
        options: {
          baseDir: 'tests/fixtures'
        },
        files: {
          'tmp/basedir_options.css': ['tests/fixtures/test.css', 'tests/fixtures/import.css']
        }
      },
      rebase_urls: {
        options: {
          baseDir: 'tests/fixtures',
          assetBaseUrl: 'static/assets/' // trailing / can be omitted
        },
        files: {
          'tmp/rebase_urls.css': ['tests/fixtures/test.css', 'tests/fixtures/import.css', 'tests/fixtures/component_a/css/style.css']
        }
      },
      no_basedir_options: {
        options: {
          assetBaseUrl: '.' // trailing / can be omitted
        },
        files: {
          'tmp/no_basedir_options.css': ['tests/fixtures/test.css', 'tests/fixtures/import.css', 'tests/fixtures/component_a/css/style.css']
        }
      },
      single_charset: {
        options: {
        },
        files: {
          'tmp/single_charset.css': ['tests/fixtures/test.css', 'tests/fixtures/charset.css', 'tests/fixtures/component_a/css/style.css']
        }
      },
      multiple_charsets: {
        options: {
        },
        files: {
          'tmp/multiple_charsets.css': ['tests/fixtures/test.css', 'tests/fixtures/charset.css', 'tests/fixtures/charset_latin-9.css', 'tests/fixtures/component_a/css/style.css']
        }
      },
      multiple_charsets_different_order: {
        options: {
        },
        files: {
          'tmp/multiple_charsets_different_order.css': ['tests/fixtures/test.css', 'tests/fixtures/charset_latin-9.css', 'tests/fixtures/charset.css', 'tests/fixtures/component_a/css/style.css']
        }
      },
      multiple_charsets_single_file: {
        options: {
        },
        files: {
          'tmp/multiple_charsets_single_file.css': ['tests/fixtures/charset_latin-9.css']
        }
      }
    },

    // Run nodeunit.
    nodeunit: {
      tests: ['tests/*_test.js']
    },
  });

  // Actually load this plugin's task(s).
  grunt.loadTasks('tasks');

  grunt.registerTask('lint', ['jshint', 'markdownlint']);
  grunt.registerTask('test', ['clean', 'concat_css', 'nodeunit']);

  // By default, lint and run all tests.
  grunt.registerTask('default', ['lint', 'test']);
};
