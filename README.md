# grunt-concat-css

[![License: MIT](https://img.shields.io/badge/license-MIT-green.svg)](https://gitlab.com/widgitlabs/nodejs/grunt-concat-css/blob/master/LICENSE)
[![Pipelines](https://gitlab.com/widgitlabs/nodejs/grunt-concat-css/badges/master/pipeline.svg)](https://gitlab.com/widgitlabs/nodejs/grunt-concat-css/pipelines)

Concat CSS with @import statements at top and relative url preserved.

## Getting Started

If you haven't used [Grunt] before, be sure to check out the [Getting Started]
guide, as it explains how to create a [Gruntfile] as well as install and use
Grunt plugins. Once you're familiar with that process, you may install this
plugin with this command:

```shell
npm install grunt-concat-css --save-dev
```

Once the plugin has been installed, it may be enabled inside your Gruntfile
with this line of JavaScript:

```js
grunt.loadNpmTasks('grunt-concat-css');
```

## The "concat_css" task

### Overview

In your project's Gruntfile, add a section named `concat_css` to the data
object passed into `grunt.initConfig()`.

```js
grunt.initConfig({
  concat_css: {
    options: {
      // Task-specific options go here.
    },
    all: {
      src: ["/**/*.css"],
      dest: "styles.css"
    },
  },
})
```

### Usage Examples

#### Default Options

By default, all css are concatenated. The only things that happens is that
every @import statement are placed at the beginning of the resulting file
(as @import statement).

```js
grunt.initConfig({
  concat_css: {
    options: {},
    files: {
      'dest/compiled.css': ['src/styles/componentA.css', 'src/styles/componentB.css'],
    },
  },
})
```

#### Rebase URLs

By specifying assetBaseUrl and baseDir, all the assets will be rebased relative
to this project rebase URL.

```js
grunt.initConfig({
  concat_css: {
    options: {
      assetBaseUrl: 'static/assets',
      baseDir: 'src/(styles|assets)'
    },
    files: {
      'static/styles.css': ['src/styles/**/*.css', 'src/assets/**/*.css']
    }
  }
})
```

## Bugs

If you find an issue, let us know [here]!

## Contributions

Anyone is welcome to contribute to the library. Please read the
[guidelines for contributing] to this repository.

There are various ways you can contribute:

1. Raise an [Issue] on GitLab
2. Send us a Pull Request with your bug fixes and/or new features
3. Provide feedback and suggestions on [enhancements]

[Grunt]: http://gruntjs.com/
[Getting Started]: http://gruntjs.com/getting-started
[Gruntfile]: http://gruntjs.com/sample-gruntfile
[here]: https://gitlab.com/widgitlabs/nodejs/grunt-concat-css/issues
[guidelines for contributing]: https://github.com/widgitlabs/nodejs/grunt-concat-css/blob/master/CONTRIBUTING.md
[enhancements]: https://gitlab.com/widgitlabs/wordpress/simple-settings/issues?label_name[]=Enhancement
[Issue]: https://gitlab.com/widgitlabs/nodejs/grunt-concat-css/issues
