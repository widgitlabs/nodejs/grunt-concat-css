# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Editorconfig support.
- Ignore file for JSHint.
- GitLab CI control file.
- Setup for linting markdown.
- Support for @charset; props [Tobbe](https://github.com/webdoc/grunt-concat-css/pull/8).
### Changed
- Move to a more standardized changelog format.
- Update .gitignore and .jshintrc.
- Rename license file.
- Update package.json file.
- Move test to tests since we have more than one.
- Use built-in normalize function; props [el-dot](https://github.com/webdoc/grunt-concat-css/pull/10).
- Clean up Gruntfile.
- Update readme.
### Fixed
- Make readme pass lint.
- Make JS pass lint.

## [0.3.2] - 2016-07-27
### Changed
- Update grunt version.

## [0.3.1] - 2014-02-03
### Changed
- Refactor to properly compute the baseUrl of assets.

## [0.3.0] - 2014-01-24
### Added
- Unit tests.
### Changed
- Replace rebaseUrls boolean with assetBaseUrl string.

## [0.2.0] - 2014-01-24
### Changed
- Disable rebaseUrls option by default.

## [0.1.1] - 2014-01-06
### Added
- New rebaseUrls option.
- New debugMode option.
### Changed
- Cleanup and improvements.

[0.3.2]: https://github.com/webdoc/grunt-concat-css/compare/8b4895b1b0e7edffb482161770097f1f7906d1c9...21bcc9c568970491439b15465e20cc9b289f52e0
[0.3.1]: https://github.com/webdoc/grunt-concat-css/compare/719ad212b996aa88516d3119ed39b1653d2ab5f8...8b4895b1b0e7edffb482161770097f1f7906d1c9
[0.3.0]: https://github.com/webdoc/grunt-concat-css/compare/c5e889ba0f75da132cdc07601a6fbb08d5691afc...719ad212b996aa88516d3119ed39b1653d2ab5f8
[0.2.0]: https://github.com/webdoc/grunt-concat-css/compare/5e26f0da6d4c0db1b83c4e2ef40fb18cd850030e...c5e889ba0f75da132cdc07601a6fbb08d5691afc
[0.1.1]: https://github.com/webdoc/grunt-concat-css/compare/1efd2669522ca85d762d662bc4e7a26d8b9ef056...5e26f0da6d4c0db1b83c4e2ef40fb18cd850030e
[0.0.1]: https://github.com/webdoc/grunt-concat-css/commit/1efd2669522ca85d762d662bc4e7a26d8b9ef056
